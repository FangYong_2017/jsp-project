/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : it-offer

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2020-09-07 14:57:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_applicant`
-- ----------------------------
DROP TABLE IF EXISTS `tb_applicant`;
CREATE TABLE `tb_applicant` (
  `APPLICANT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `APPLICANT_EMAIL` varchar(50) NOT NULL,
  `APPLICANT_PWD` varchar(50) NOT NULL,
  `APPLICANT_REGISTDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`APPLICANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_applicant
-- ----------------------------
INSERT INTO `tb_applicant` VALUES ('1', 'qst@itoffer.cn', '123456', '2020-06-21 18:46:28');
INSERT INTO `tb_applicant` VALUES ('2', 'abc@qq.com', '123456', '2020-06-24 09:18:02');
INSERT INTO `tb_applicant` VALUES ('3', '123@qq.com', '123456', '2020-06-30 14:03:32');

-- ----------------------------
-- Table structure for `tb_company`
-- ----------------------------
DROP TABLE IF EXISTS `tb_company`;
CREATE TABLE `tb_company` (
  `COMPANY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_NAME` varchar(50) DEFAULT NULL,
  `COMPANY_AREA` varchar(50) DEFAULT NULL,
  `COMPANY_SIZE` varchar(50) DEFAULT NULL,
  `COMPANY_TYPE` varchar(50) DEFAULT NULL,
  `COMPANY_BRIEF` varchar(2000) DEFAULT NULL,
  `COMPANY_STATE` int(2) DEFAULT NULL,
  `COMPANY_SORT` int(2) DEFAULT NULL,
  `COMPANY_VIEWNUM` mediumtext,
  `COMPANY_PIC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`COMPANY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_company
-- ----------------------------
INSERT INTO `tb_company` VALUES ('1', '凌志软件股份有限公司', '苏州市', '1000人以上', '股份制企业', '凌志软件股份有限公司成立于2003年1月，是一家致力于为各行业提供软件产品研发，软件外包开发及IT系统集成服务。业务范围包括证券，银行，保险，电子商务，物流等。金融行业产品研发及软件外包服务的专业性及稳定性，已成为企业的核心竞争力。公司在日本东京、上海、北京、深圳等地均设有分支机构。凌志软件经过10年多的发展，得到现有海内外客户高度认可，并在中国和日本形成了一定的品牌知名度，海外业务以日本市场为核心，已成为国际知名企业的核心供应商，在2011年软件出口企业排行榜中名列第10名，并获得2011-2012年国家规划布局内重点软件企业称号。凌志软件在稳步扩大高端软件外包业务的同时，自主研发国内高端金融产品，现已申请多项发明专利并开发了多款拥有自主知识产权的金融软件产品，投入商业应用并得到客户的高度评价，逐步在国内市场上崭露头角。', '1', '1', '1401', '635170123249913750.jpg');
INSERT INTO `tb_company` VALUES ('2', '苏州大宇宙信息创造有限公司', '苏州市', '100-200人', '外资企业', '苏州大宇宙信息创造有限公司成立于2008年10月，是大宇宙信息创造（中国）有限公司全资子公司，注册资金为1600万元。公司位于风景优美的中国新加坡合作苏州工业园区独墅湖高教区，拥有自己的办公及研发大楼，是园区重点引进的软件服务外包企业。公司是一家专业从事国际和国内企事业信息化解决方案、软件外包的高科技企业，为国内外企业提供一流的软件开发、系统集成及维护、客户支持等综合的信息服务。公司拥有一支高素质的管理与开发团队，具有良好的外语能力和丰富的软件设计开发经验，同时具备与国内外客户的良好商务沟通能力。公司成立至今，保持着稳健发展的势头，事业日益发展和壮大，目前已与国内外多家企业建立了长期稳定的客户关系。公司于2009年7月份顺利通过ISO27001信息安全管理的国际认证，2010年6月顺利通过CMMI3级认证。公司具备完善的管理、教育培训和薪酬福利体系以及健全的规章制度，为员工的工作和学习提供了广阔、自由的发展空间。', '1', '2', '583', '635508802169230812.jpg');
INSERT INTO `tb_company` VALUES ('3', '北京日立华胜信息系统有限公司', '东城区', '200-500人', '合资企业', '北京日立华胜信息系统有限公司（简称BHH）是世界五百强之一的HITACHI日立集团和信息产业部电子六所共同投资设立的高新技术企业。公司主要从事对日软件开发,自成立以来，我们承接了日本各大银行?证券交易所相关系统、日本新干线铁路座位预约系统、面向日本政府机关的财务会计系统/税金管理系统/居民信息管理系统、纳税系统、生产管理系统、销售管理系统、日本各大汽车厂商的ECU软件、信息终端设备软件等各种大型软件开发项目。业务领域涵盖：金融、产业/流通、公共政府、ATM以及嵌入式五大领域。从1996年起，公司便已经开始从事汽车引擎控制、变速器控制、自动巡航控制等领域的嵌入式软件开发，积累了丰富的嵌入式软件的开发经验。公司十分注重对员工的外语及业务技能培训，提供多次出国工作机会和充分的发展空间；公司员工均享有良好的薪资和完备的福利保险待遇（“五险一金”和补充医疗/意外伤害保险，以及多项补贴）。诚挚邀请有志于从事对日软件开发、德才兼备的毕业生加盟，开辟属于自己的崭新生活。欢迎各位有识之士的加盟。', '1', '3', '1184', '635086129655240312.jpg');
INSERT INTO `tb_company` VALUES ('4', '青岛百灵信息科技有限公司', '东城区', '200-500人', '合资企业', '青岛百灵信息科技有限公司。', '1', '3', '1183', '635061323749843750.jpg');
INSERT INTO `tb_company` VALUES ('6', '易科德软件有限公司', '东城区', '200-500人', '合资企业', '易科德软件有限公司', '1', '3', '1183', '635062208049218750.jpg');
INSERT INTO `tb_company` VALUES ('7', '大连和众信拓科技有限公司', '东城区', '200-500人', '合资企业', '大连和众信拓科技有限公司', '1', '3', '1183', '635084591046656250.jpg');
INSERT INTO `tb_company` VALUES ('8', 'NTT DATA', '东城区', '200-500人', '合资企业', 'NTT DATA', '1', '3', '1183', '635169304955382500.jpg');
INSERT INTO `tb_company` VALUES ('9', '萨纳斯科技有限公司', '东城区', '200-500人', '合资企业', '萨纳斯科技有限公司', '1', '3', '1184', '635284036333940135.jpg');
INSERT INTO `tb_company` VALUES ('10', '仕德伟科技有限公司', '东城区', '200-500人', '合资企业', '仕德伟科技有限公司', '1', '3', '1183', '635386133707515461.jpg');
INSERT INTO `tb_company` VALUES ('11', '无锡晟奥软件有限公司', '东城区', '200-500人', '合资企业', '无锡晟奥软件有限公司', '1', '3', '1183', '635508636209238443.jpg');
INSERT INTO `tb_company` VALUES ('12', '宏智科技有限公司', '东城区', '200-500人', '合资企业', '宏智科技有限公司', '1', '3', '1183', '635508801853812771.jpg');
INSERT INTO `tb_company` VALUES ('13', '苏州大宇宙信息科创有限公司', '东城区', '200-500人', '合资企业', '苏州大宇宙信息科创有限公司', '1', '3', '1183', '635508802169230812.jpg');

-- ----------------------------
-- Table structure for `tb_job`
-- ----------------------------
DROP TABLE IF EXISTS `tb_job`;
CREATE TABLE `tb_job` (
  `JOB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_ID` int(11) NOT NULL,
  `JOB_NAME` varchar(100) DEFAULT NULL,
  `JOB_HIRINGNUM` int(11) DEFAULT NULL,
  `JOB_SALARY` varchar(20) DEFAULT NULL,
  `JOB_AREA` varchar(255) DEFAULT NULL,
  `JOB_DESC` varchar(255) DEFAULT NULL,
  `JOB_ENDTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `JOB_STATE` int(2) DEFAULT NULL,
  PRIMARY KEY (`JOB_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_job
-- ----------------------------
INSERT INTO `tb_job` VALUES ('1', '1', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('2', '2', 'Java软件开发工程师（提供岗前培训）', '20', '2500~4000元/月', '工业园区', '针对客户现场软件问题提供技术支持，包括在研发实验室重现客户软件问题，分析代码问题原因，提供解决方案，并测试更新的代码符合客户要求', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('3', '3', '对日软件开发（提供岗前培训）', '40', '4万-4.5万/年', '历城区', 'J2EE开发 TOMCAT/JBOSS等主流应用服务器 Webservice、SOCKET、SNMP等标准接口和协议,Struts2、Spring、Hibernate等常用框架 Linux操作系统及oracle。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('4', '4', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('5', '5', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('6', '6', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('7', '7', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('8', '8', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('9', '9', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('10', '10', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('11', '11', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('12', '12', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('13', '13', '对日软件开发工程师（提供岗前培训）', '100', '2500~4000元/月', '吴中区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');
INSERT INTO `tb_job` VALUES ('14', '1', '对日软件开发工程师', '1000', '25000~40000元/月', '大港区', '担任设计书的制作、程序开发、测试实施等工作。', '2016-03-05 00:00:00', '1');

-- ----------------------------
-- Table structure for `tb_jobapply`
-- ----------------------------
DROP TABLE IF EXISTS `tb_jobapply`;
CREATE TABLE `tb_jobapply` (
  `APPLY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `JOB_ID` int(11) NOT NULL,
  `APPLICANT_ID` int(11) NOT NULL,
  `APPLY_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `APPLY_STATE` int(2) DEFAULT NULL,
  PRIMARY KEY (`APPLY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_jobapply
-- ----------------------------
INSERT INTO `tb_jobapply` VALUES ('1', '1', '1', '2015-03-05 00:00:00', '1');
INSERT INTO `tb_jobapply` VALUES ('2', '14', '2', '2020-06-30 08:44:51', '2');
INSERT INTO `tb_jobapply` VALUES ('3', '1', '2', '2020-06-30 08:44:56', '4');
INSERT INTO `tb_jobapply` VALUES ('4', '1', '2', '2020-06-29 09:17:26', '1');
INSERT INTO `tb_jobapply` VALUES ('5', '14', '2', '2020-06-30 08:44:59', '5');
INSERT INTO `tb_jobapply` VALUES ('6', '14', '2', '2020-06-30 08:45:00', '3');
INSERT INTO `tb_jobapply` VALUES ('7', '14', '2', '2020-06-30 08:45:02', '3');
INSERT INTO `tb_jobapply` VALUES ('8', '4', '2', '2020-06-30 08:45:03', '2');

-- ----------------------------
-- Table structure for `tb_resume_basicinfo`
-- ----------------------------
DROP TABLE IF EXISTS `tb_resume_basicinfo`;
CREATE TABLE `tb_resume_basicinfo` (
  `BASICINFO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `APPLICANT_ID` int(11) DEFAULT NULL,
  `REALNAME` varchar(50) NOT NULL,
  `GENDER` varchar(50) NOT NULL,
  `BIRTHDAY` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CURRENT_LOC` varchar(255) DEFAULT NULL,
  `RESIDENT_LOC` varchar(255) DEFAULT NULL,
  `TELEPHONE` varchar(50) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `JOB_INTENSION` varchar(50) DEFAULT NULL,
  `JOB_EXPERIENCE` varchar(255) DEFAULT NULL,
  `HEAD_SHOT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BASICINFO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_resume_basicinfo
-- ----------------------------
INSERT INTO `tb_resume_basicinfo` VALUES ('1', '1', '张三', '男', '1993-11-05 00:00:00', '山东省青岛市高新区', '山东省青岛市', '13166666666', 'qst@itoffer.cn', 'Java软件开发', '刚刚参加工作', null);
INSERT INTO `tb_resume_basicinfo` VALUES ('3', '2', '方勇', '男', '2020-06-30 13:58:09', '12312', '123123', '13126057168', 'qst@itoffer.cn', '123123', '刚刚参加工作', '1593496689022.jpg');
INSERT INTO `tb_resume_basicinfo` VALUES ('4', '3', '方勇', '男', '2020-06-30 14:16:17', '12312', '123123', '13126057168', '123@qq.com', '123123', '刚刚参加工作', '1593497777284.jpg');
