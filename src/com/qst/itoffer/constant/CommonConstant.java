package com.qst.itoffer.constant;

public class CommonConstant {
	
	//用户登录的session名称
	public static final String SESSION_APPLICANT ="SESSION_APPLICANT";
	
	//用户简历id存储的session名称
	public static final String SESSION_RESUMEID ="SESSION_RESUMEID";

}
